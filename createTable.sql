CREATE TABLE `registry` (
  `nome` varchar(45) NOT NULL,
  `cognome` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `datascadenza` datetime NOT NULL,
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `recipt` (
  `number` mediumint(9) NOT NULL,
  `user_id` mediumint(9) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `description` varchar(500) NOT NULL,
  PRIMARY KEY (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
