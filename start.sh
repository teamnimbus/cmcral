#!/usr/bin/env bash

/usr/lib/java/jdk1.8.0_131/bin/java -cp "$HOME/cmcral/code/lib/*:$HOME/cmcral/code" cm/Main &
pid=$!
echo $pid > ~/cmcral/cm.pid
echo "Started (PID $pid)"