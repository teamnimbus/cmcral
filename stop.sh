#!/usr/bin/env bash


function is_running() {
  kill -s 0 "$1" 2>/dev/null
}

if [ -f ~/cmcral/cm.pid ]; then
    pid="$(cat ~/cmcral/cm.pid)"
    echo -n "Terminating application (PID $pid)"
    is_running "$pid" && kill "$pid" 2>/dev/null

    i=0
    while is_running "$pid" && [[ $((i++)) -le 100 ]]; do
      echo -n "."
      sleep 0.1
    done
    if is_running "$pid"; then
      kill -9 "$pid" 2>/dev/null
      echo -n "(killed)"
    fi
    echo

    rm -f ~/cmcral/cm.pid
  fi