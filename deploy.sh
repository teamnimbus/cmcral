#! /bin/bash

cd ~/cmcral/
rm -fr out/
mkdir out/
mkdir out/lib
mkdir out/Assets

/home/andreabasile/Downloads/jdk1.8.0_161/bin/javac -cp "./lib/*"  -d ./out/ ./src/*/*.java
cp -r ./Assets/* ./out
cp -r ./lib/* ./out/lib/
cp -r ./src/log4j2.xml ./out/

scp -qr ./stop.sh xpuser@cmcral.p7intranet.it:/home/xpuser/
ssh xpuser@cmcral.p7intranet.it "~/stop.sh" > /dev/null
ssh xpuser@cmcral.p7intranet.it "rm -fr ~/cmcral/*"
ssh xpuser@cmcral.p7intranet.it "mkdir -p cmcral"
ssh xpuser@cmcral.p7intranet.it "mkdir -p cmcral/code"
scp -qr ./out/* xpuser@cmcral.p7intranet.it:/home/xpuser/cmcral/code/
scp -qr ./start.sh xpuser@cmcral.p7intranet.it:/home/xpuser/
ssh xpuser@cmcral.p7intranet.it "~/start.sh" > /dev/null &