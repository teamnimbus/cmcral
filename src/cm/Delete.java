package cm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class Delete {

    public Delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        new Connection().executeUpdate("DELETE FROM registry WHERE id= ?",
                Arrays.asList((Integer) Integer.parseInt((request.getParameter("id1")))));
        response.sendRedirect(Handler.list);
    }
}