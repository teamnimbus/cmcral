package cm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class createReciptAction {

    public createReciptAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

        ResultSet result = new Connection().execute("SELECT * FROM registry WHERE id = ?",
                Arrays.asList(Integer.parseInt(request.getParameter("id"))));
        result.next();

        String price = request.getParameter("price");
        String description = request.getParameter("description");
        String nome = result.getString("nome");
        String cognome = result.getString("cognome");

        int reciptId = new Recipt().Generate(result.getInt("id"), description, new BigDecimal(price));

        InputStream inputStream = Pdf.generateFrom(Html(reciptId, price, description, nome, cognome));


        new Mailer("cral@7pixel.it",
                result.getString("email"),
                "cral@7pixel.it",
                "Ricevuta",
                "Ciao, trovi la ricevuta in allegato.")
                .WithAttach(inputStream, "ricevuta.pdf", "application/pdf")
                .send();

        response.sendRedirect("/");

    }

    private String Html(int reciptId, String price, String Description, String name, String lastname) {
        return "" +
                "<!DOCTYPE html>" +
                "<html lang=\"en\">" +
                "  <head>" +
                "    <meta charset=\"utf-8\">" +
                "    <link rel=\"stylesheet\" href=\"https://s3-eu-west-1.amazonaws.com/htmlpdfapi.production/free_html5_invoice_templates/example1/style.css\" media=\"all\" />" +
                "  </head>" +
                "  <body>" +
                "    <header class=\"clearfix\">" +
                "      <div id=\"logo\">" +
                "        <img src=\"https://storage.googleapis.com/7pixel/6c395b8d-b820-4173-8789-f4fbb206e235.jpg\" style=\"height: 50; width: 60;\">" +
                "      </div>" +
                "      <h1>RICEVUTA NUMERO "+ reciptId +"</h1>" +
                "      <div id=\"project\">" +
                "        <div>BENEFICIARIO: CRAL 7Pixel</div>" +
                "        <div>Da: "+ name + " " + lastname + "</div>" +
                "      </div>" +
                "    </header>" +
                "    <main>" +
                "      <table>" +
                "        <thead>" +
                "          <tr>" +
                "            <th class=\"service\">Data</th>" +
                "            <th class=\"desc\">Causale</th>" +
                "            <th class=\"desc\"></th>" +
                "            <th class=\"desc\"></th>" +
                "            <th>Importo</th>" +
                "          </tr>" +
                "        </thead>" +
                "        <tbody>" +
                "          <tr>" +
                "            <td class=\"service\">"+new SimpleDateFormat("dd-MM-yyyy").format(new Date())+"</td>" +
                "            <td class=\"desc\">"+Description+"</td>" +
                "            <td class=\"unit\"></td>" +
                "            <td class=\"qty\"></td>" +
                "            <td class=\"total\">"+ price +" €</td>" +
                "          </tr>" +
                "          <tr>" +
                "            <td colspan=\"4\" class=\"grand total\">TOTALE</td>" +
                "            <td class=\"grand total\">" + price + " €</td>" +
                "          </tr>" +
                "        </tbody>" +
                "      </table>" +
                "      <div id=\"notices\">" +
                "      </div>" +
                "    </main>" +
                "    <footer>" +
                "    </footer>" +
                "  </body>" +
                "</html>";
    }
}
