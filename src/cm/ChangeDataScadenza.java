package cm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class ChangeDataScadenza {
    public ChangeDataScadenza(HttpServletRequest request, HttpServletResponse response) throws IOException, ParseException {

        new Connection().executeUpdate("UPDATE registry SET datascadenza = ? WHERE id = ?",
                Arrays.asList(getDate(request.getParameter("ndatascadenza")),
                        Integer.parseInt(request.getParameter("id"))));
        response.sendRedirect(Handler.list);
    }

    private Date getDate(String dataScadenza) throws ParseException {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(dataScadenza);
    }
}
