package cm;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

public class Recipt {
    public int Generate(int user_id, String description, BigDecimal price) throws SQLException {
        int number = 0;
        synchronized (this){
            ResultSet execute = new Connection().execute("SELECT number FROM recipt ORDER BY number DESC", new ArrayList<>());
            if(execute.next()) {
                number = execute.getInt("number") + 1;
            }
            new Connection().executeUpdate("INSERT INTO recipt (number, user_id, description, price) VALUE (?, ?, ?, ?)",
                    Arrays.asList(number, user_id, description, price));
        }
        return number;

    }
}
