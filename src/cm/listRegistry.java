package cm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class listRegistry {

    public listRegistry(HttpServletRequest request, HttpServletResponse response) throws IOException {
        List<Registry> list = new ArrayList<>();

        ResultSet rs = new Connection().execute("Select * FROM registry order by datascadenza", Arrays.asList());
        try {

            while(rs.next()) {
             list.add(new Registry(rs.getString("nome"),
                                     rs.getString("cognome"),
                                     rs.getString("email"),
                                     rs.getDate("datascadenza"),
                                     rs.getInt("ID")));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Map<String, Object> conf =  new HashMap<>();
        conf.put("list",list);
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().print(
                DisplayAction.template().render("Index.html", conf));
    }
}
