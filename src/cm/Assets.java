package cm;

import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletHolder;

public class Assets {

    ServletHolder resource() {
        ServletHolder assetsHolder = new ServletHolder("default", new DefaultServlet());
        assetsHolder.setInitParameter("resourceBase", this.getClass().getResource("../").getPath());
        assetsHolder.setInitParameter("pathInfoOnly","true");
        return assetsHolder;
    }
}
