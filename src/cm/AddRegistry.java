package cm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class AddRegistry {


    public AddRegistry(HttpServletRequest request, HttpServletResponse response) throws IOException {

        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        HashMap<String, Object> params = new HashMap<>();
        params.put("add", Boolean.valueOf(request.getParameter("addelement")));
        params.put("error", Boolean.valueOf(request.getParameter("error")));
        response.getWriter().print(DisplayAction.template().render("Add.html", params));
    }
}
