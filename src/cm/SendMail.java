package cm;

import org.apache.commons.lang3.time.DateUtils;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;

public class SendMail {
    public SendMail(HttpServletRequest request, HttpServletResponse response) throws IOException {

        try {
            ResultSet execute = new Connection().execute("SELECT * FROM registry " +
                                                                "WHERE datascadenza >= date(?) " +
                            "AND datascadenza < date(?)",
                Arrays.asList(DateUtils.addDays(new Date(), 30),
                        DateUtils.addDays(new Date(), 31)));
            while(execute.next()) {
                String email = execute.getString("email");
                new Mailer("cral@7pixel.it",
                    email,
                    "cral@7pixel.it",
                    "Certificato Medico di " + execute.getString("nome") +" "+ execute.getString("cognome"),
                        body(execute.getDate("datascadenza")))
                        .send();
                response.getWriter().print("Mail mandata con successo a: " + email);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private String body(java.sql.Date datascadenza) {
        return "Ciao," +
                "<br>" +
                "<br>" +
                "ti ricordiamo che il tuo certificato medico scadr&agrave; in data " + datascadenza.toString() +
                "<br>" +
                "Ti chiediamo la cortesia, appena l&lsquo;avrai rinnovato, di farcene avere una copia." +
                "<br>" +
                "<br>" +
                "Grazie." +
                "<br>" +
                "Direttivo CRAL";
    }
}
