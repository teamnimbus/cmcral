package cm;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;

public class Main{

    public static void main(String[] args ) throws Exception {

        Server server = new Server(7000);
        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(Handler.class, "/*");
        handler.addServlet(new Assets().resource(), "/Assets/*");
        server.setHandler(handler);
        server.start();
        server.join();
    }
}






