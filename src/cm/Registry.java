package cm;

import java.sql.Date;

public class Registry {
    private String nome;
    private String cognome;
    private String email;
    public Date datascadenza;
    public int ID;

    public Registry(String nome, String cognome, String email, Date datascadenza, int id) {
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
        this.datascadenza = datascadenza;
        this.ID=id;
    }

    public String getNome(){return nome;}

    public String getCognome() {
        return cognome;
    }

    public String getEmail() {return email;}

    public Date getDatascadenza() {return datascadenza;}

    public int getID(){return ID;}

}

