package cm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class WriteRegistry {

    public WriteRegistry(HttpServletRequest request, HttpServletResponse response) throws IOException, ParseException, SQLException {


        Connection connection = new Connection();
        ResultSet result = connection.execute("SELECT COUNT(email) as counter FROM registry WHERE email = ?",
                Arrays.asList(request.getParameter("email")));
        result.next();
        if(result.getInt("counter") > 0){
            response.sendRedirect(Handler.AGGIUNGI+"?error=true");
            return;
        }

        connection.executeUpdate("INSERT INTO registry( nome, cognome, email, datascadenza) " +
                "VALUES (?, ?, ?, ?)",
                Arrays.asList(request.getParameter("nome"),
                        request.getParameter("cognome"),
                        request.getParameter("email"),
                        getDate(request.getParameter("datascadenza"))));

        response.sendRedirect(Handler.AGGIUNGI+"?addelement=true");
    }

    private Date getDate(String datascadenza) throws ParseException {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(datascadenza);
    }
}

