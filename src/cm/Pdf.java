package cm;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class Pdf {

    public static final String API_KEY = "6c06d472-b750-4578-8564-7ec9156d3632";
    public static final String BASE_URL = "http://api.html2pdfrocket.com/pdf";

    public static InputStream generateFrom(String html) throws Exception {

        return DownloadFile(API_KEY, html, "");
    }

    private static InputStream DownloadFile(String APIKey, String value, String ExtraParams) throws Exception {
        URL url;
        String Params = "";
        if (ExtraParams != null && !"".equals(ExtraParams)) {
             Params = ExtraParams;
             if (!Params.substring(0, 1).equals("&")) {
                 Params = "&" + Params;
             }
         }

        value = URLEncoder.encode(value, java.nio.charset.StandardCharsets.UTF_8.toString());
        value += Params;

        // Validate parameters
        if (APIKey == null || "".equals(APIKey)) throw (new Exception("API key is empty"));

        // Append parameters for API call
        url = new URL(BASE_URL + "?apikey=" + APIKey + "&value=" + value);

        // Download PDF file
        URLConnection connection = url.openConnection();
        return connection.getInputStream();
    }

    private static void saveFile(String Filename, InputStream instream) throws IOException {
        // Write PDF file
        BufferedInputStream BISin = new BufferedInputStream(instream);
        FileOutputStream FOSfile = new FileOutputStream(Filename);
        BufferedOutputStream out = new BufferedOutputStream(FOSfile);

        int i;
        while ((i = BISin.read()) != -1) {
            out.write(i);
        }

        // Clean up
        out.flush();
        out.close();
        System.out.println("File " + Filename + " created");
    }
}
