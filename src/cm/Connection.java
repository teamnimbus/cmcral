package cm;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Connection {

    public ResultSet execute(String query, List<Object> params){

        try {
            new com.mysql.jdbc.Driver();
            java.sql.Connection connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/cm?useSSL=false&user=root&password=password");

            PreparedStatement ps = connect.prepareStatement(query);
            for (int i = 0; i < params.size(); i++) {
                ps.setObject(i + 1, params.get(i));
            }

            return ps.executeQuery();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int executeUpdate(String query, List<Object> params){

        try {
            new com.mysql.jdbc.Driver();
            java.sql.Connection connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/cm?user=root&password=password");

            PreparedStatement ps = connect.prepareStatement(query);

            for (int i = 0; i < params.size(); i++) {
                ps.setObject(i + 1, params.get(i));
            }

            return ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
