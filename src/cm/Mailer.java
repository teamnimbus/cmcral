package cm;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Mailer {
    private final MimeMessage message;

    public Mailer(String from, String to, String bcc, String subject, String text) throws MessagingException {

        Properties props = System.getProperties();

        props.put("mail.smtp.host", "mail.trovaprezzi.it");

        message = new MimeMessage(Session.getInstance(props, null));

        message.setFrom(new InternetAddress(from));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));

        message.setSubject(subject);
        message.setContent(text, "text/html");
    }

    public Mailer WithAttach(InputStream inputStream, String fileName, String fileType) throws MessagingException, IOException {

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        Multipart multipart = new MimeMultipart();
        ByteArrayDataSource source = new ByteArrayDataSource(inputStream, fileType);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(fileName);
        multipart.addBodyPart(messageBodyPart);

        message.setContent(multipart);

        return this;
    }

    public void send() throws MessagingException {

        Transport.send(message);
    }
}
