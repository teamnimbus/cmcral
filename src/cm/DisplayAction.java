package cm;

import org.rythmengine.RythmEngine;

import java.util.HashMap;
import java.util.Map;

public class DisplayAction {
    private static RythmEngine _rythm;

    static RythmEngine template() {
        if(_rythm == null) {
            Map<String, Object> map = new HashMap<>();
            map.put("home.template", "view");
            _rythm = new RythmEngine(map);
        }

        return _rythm;
    }
}
