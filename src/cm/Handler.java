package cm;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Handler extends HttpServlet
{

    public static final String AGGIUNGI = "/aggiungi";
    final static Logger _logger = LogManager.getLogger(Handler.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {

        try {
            handle(req, resp);
        } catch (Throwable t) {
            _logger.error("Exception thrown for request '" + req.getRequestURL() + "'" + " Query string: " + req.getQueryString(), t);
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        try {
            handle(req, resp);
        } catch (Throwable t) {
            _logger.error("Exception thrown for request '" + req.getRequestURL() + "'" + " Query string: " + req.getQueryString(), t);
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    public static final String list = "/";
    public static final String WRITE = "/write";
    public static final String DELETE = "/delete";
    public static final String SEND_MAIL = "/send-mail";
    public static final String CHANGEDATA = "/changedata";
    public static final String CREATE_RECIPT = "/create-recipt";


    public void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {

        if(request.getRequestURI().equals(DELETE)) {
            new Delete(request, response);
        }
        if (request.getRequestURI().equals(SEND_MAIL)){
            new SendMail(request, response);
        }
        if(request.getRequestURI().equals(WRITE)){
            new WriteRegistry(request, response);
        }
        if (request.getRequestURI().equals(AGGIUNGI)){
            new AddRegistry(request, response);
        }
        if (request.getRequestURI().equals(list)){
            new listRegistry(request, response);
        }
        if (request.getRequestURI().equals(CHANGEDATA)){
            new ChangeDataScadenza(request, response);
        }
        if (request.getRequestURI().equals(CREATE_RECIPT)){
            new createReciptAction(request, response);
        }
    }
}
